package com.devcamp.api.rainbow_api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RainbowApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(RainbowApiApplication.class, args);
	}

}
